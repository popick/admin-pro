search
===
```sql
select  u.id as user_id, username, password, uname, sex, phone, email, u.create_time, 
              r.id, rname, power, description 
 FROM (select * from user_info as u
where 1
-- @if(!isEmpty(user.username)){
	and username like #{"%"+user.username+"%"}
-- @}
-- @if(!isEmpty(user.uname)){
	and uname like #{"%"+user.uname+"%"}
-- @}
-- @if(!isEmpty(user.sex)){
	and sex = #{user.sex}
-- @}
-- @if(!isEmpty(user.phone)){
	and phone like #{"%"+user.phone+"%"}
-- @}
-- @if(!isEmpty(user.email)){
	and email like #{"%"+user.email+"%"}
-- @}
-- @if(!isEmpty(user.id)){
	and u.id = #{user.id}
-- @}
limit #{(page-1)*pageCount},#{pageCount}
) as u
  LEFT JOIN user_role ur ON u.id = ur.user_id 
  LEFT JOIN role_info r ON r.id = ur.role_id 
order by u.id asc, r.power asc
```

searchCount
===
```sql
select count 
 FROM user_info u 
 where 1
-- @if(!isEmpty(user.username)){
	and username like #{"%"+user.username+"%"}
-- @}
-- @if(!isEmpty(user.uname)){
	and uname like #{"%"+user.uname+"%"}
-- @}
-- @if(!isEmpty(user.sex)){
	and sex = #{user.sex}
-- @}
-- @if(!isEmpty(user.phone)){
	and phone like #{"%"+user.phone+"%"}
-- @}
-- @if(!isEmpty(user.email)){
	and email like #{"%"+user.email+"%"}
-- @}
-- @if(!isEmpty(user.id)){
	and u.id = #{user.id}
-- @}
```
searchAndRole
===

```sql
SELECT user_id, username, password, uname, sex, phone, email, 
              r.id, rname, power, description FROM (
   SELECT u.id id, username, `password`, uname, sex, phone, email,rid FROM (
  SELECT u.id id, username, `password`, uname, sex, phone, email, 
   (SELECT  GROUP_CONCAT(r.id) FROM user_role ur,role_info r WHERE ur.role_id=r.id AND ur.user_id=u.id) rid
    FROM user_info u) AS u
 WHERE 1
-- @for(rid in rids){
    and FIND_IN_SET(#{rid},rid)
-- @}
LIMIT #{(page-1)*pageCount},#{pageCount}
) user
LEFT JOIN user_role ur ON user.id = ur.user_id 
LEFT JOIN role_info r ON r.id = ur.role_id
```

searchAndRoleCount
===
```sql
SELECT COUNT(1) FROM (
  SELECT u.id id, username, `password`, uname, sex, phone, email, u.create_time, 
   (SELECT  GROUP_CONCAT(r.id) FROM user_role ur,role_info r WHERE ur.role_id=r.id AND ur.user_id=u.id) rid
    FROM user_info u
) AS u
 WHERE
FIND_IN_SET(1,rid)
```
