UserAndRole
===

*User
```json
{
"id" : "user_id",
"username" : "username",
"password" : "password",
"uname" : "uname",
"sex" : "sex",
"phone" : "phone",
"email" : "email",
"createTime" : "create_time",
"updateTime" : "update_time",
"roleList" : 
   { 
     "id" : "id",
     "rname" : "rname",
     "power" : "power",
     "description" : "description"
   }
}
```
