selectAllUser
===
```sql
SELECT u.id as user_id, username, password, uname, sex, phone, email, u.create_time, 
       r.id, rname, power, description 
FROM (select * from user_info order by id asc limit #{(page-1)*pageCount},#{pageCount}) u 
           LEFT JOIN user_role ur ON u.id = ur.user_id 
           LEFT JOIN role_info r ON r.id = ur.role_id 
order by u.id asc, r.power asc
```

existUsername
===
```sql
select id from user_info where username = #{username}
```
