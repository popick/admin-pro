selectByName
===
```sql
select u.id as user_id, username, password, uname, sex, phone, email, u.create_time,
       r.id, rname, power, description 
from user_info u, user_role ur, role_info r
where u.id = ur.user_id and r.id = ur.role_id and username =#{username} and password = #{password}
```

insertUandt
===
```sql
insert into user_info(username,password,uname,sex,phone,email,create_time,update_time) values(#{user.username},#{user.password},#{user.uname},#{user.sex},#{user.phone},#{user.email},#{user.createTime},#{user.updateTime})
```
