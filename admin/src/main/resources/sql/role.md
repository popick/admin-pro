selectAllRoleMess
===
```sql
select r.id,r.rname,r.description,r.power,COUNT(ur.role_id) countPeople
from role_info r
LEFT JOIN user_role ur ON r.id = ur.role_id
GROUP BY r.id
```

deleteUserRoleInfoByRoleId
===
```sql
delete from user_role where role_id = #{id}
```

delManyRoleById
===
```sql
delete from role_info where id in (
    -- @for(id in list){
    #{id}  #{text(idLP.last?"":"," )}
    -- @}
    )
```

delManyRoleAndUserByRoleId
===
```sql
delete from user_role where role_id in (
    -- @for(id in list){
    #{id}  #{text(idLP.last?"":"," )}
    -- @}
    )
```

findRoleByUserId
===
```sql
SELECT * from role_info r where r.id in (select role_id  from user_role  where user_id = 1)
```