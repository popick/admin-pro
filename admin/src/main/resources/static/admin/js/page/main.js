layui.use(['element', 'carousel'], function () {
    let element = layui.element;
    let carousel = layui.carousel;
    carousel.render({
        elem: '#id_admin-carousel1'
        , width: '100%' //设置容器宽度
        , arrow: 'none'
        , trigger: 'mouseenter'
        , autoplay: false
        , height: '180px'
    });
    carousel.render({
        elem: '#id_admin-carousel2'
        , width: '100%' //设置容器宽度
        , arrow: 'none'
        , trigger: 'mouseenter'
        , autoplay: false
        , height: '180px'
    })
    carousel.render({
        elem: '#id_admin-carousel3'
        , width: '100%' //设置容器宽度
        , arrow: 'none'
        , trigger: 'mouseenter'
        , autoplay: false
        , height: '400px'
    })

})