$(function (){
    jumpPage('main');
})

$(window).on('resize', function() {
    AdminInit();
    // iframe窗口自适应
    var $content = $('.layui-tab-content');
    $content.height($(this).height() - 125);
    $content.find('iframe').each(function() {
        $(this).height($content.height());
    });
}).resize();

function AdminInit() {
    //layui-fluid 为外层div
    $('.layui-fluid').height($(window).height());
    $('body').height($(window).height());
}

layui.use(['element','layer'], function () {
    let element = layui.element
        ,layer = layui.layer
        ,$ = layui.jquery;

    element.on('nav(navTree)', function (elem) {
        if($(this).attr("lay-href") !== undefined){
            let flag = true;
            let url = $(this).attr("lay-href");

            $('.layui-tab-title li').each(function (i,e) {
                if($(this).attr("lay-id") === url){
                    element.tabChange('main_content', url);
                    flag = false;
                }
            })

            if(flag){
                element.tabAdd('main_content', {
                    title: elem[0].innerText,
                    content: '<iframe scrolling="yes" frameborder="0" src="page/'+url+'" style="width:100%;height:1000px"></iframe>',
                    id: url
                });
                element.tabChange('main_content', url)
            }
            location.hash = '/page/'+ url;
        }
    })
})



function jumpPage(name) {
    $.ajax({
        url: 'page/' + name
        , type: 'GET'
        , success: function (data) {
            if(data[8] == 1){
                location.href="page/login"
            }
        }
    })
}