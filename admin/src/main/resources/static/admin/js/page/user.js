layui.config({
    version: '1606406696877' //为了更新 js 缓存，可忽略
});
layui.use(['table', 'layer', 'form'], function () {

    let table = layui.table;
    let layer = layui.layer;
    let form = layui.form;

    //表格渲染
    let tableIns = table.render({
        elem: '#user_table',
        title: '用户表',
        url: 'user/selectAll',
        loading: true,
        where:{
        },
        totalRow: true,
        toolbar: '#admin_toolbar',
        page: true,
        cols: [[
            {type: 'checkbox', fixed: 'left'},
            {field: 'id', title: 'ID', fixed: 'left', width: '5%'},
            {field: 'username', title: '用户名', width: '8.5%'},
            {field: 'uname', title: '姓名', width: '8%'},
            {
                field: 'sex', title: '性别', width: '5%', templet: function (data) {
                    return data.sex ? '男' : '女';
                }
            },
            {field: 'phone', title: '手机', width: '10%'},
            {field: 'email', title: '邮箱', width: '15%'},
            {field: 'createTime', title: '加入时间', width: '15%'},
            {
                field: 'roleList', title: '角色', width: '20%', templet: function (data) {
                    console.log(data.roleList)
                    let roleList = data.roleList;
                    let res = "";
                    for (let i = 0; i < roleList.length; i++) {
                        let rname = roleList[i].rname;
                        if (i === roleList.length - 1) {
                            res += rname;
                            break;
                        }
                        res += rname + ", ";
                    }

                    return res;
                }
            },
            {field: 'component', title: '操作', width: '10%', templet: '#admin_tool'}
        ]],
        parseData: function (res) {
            return {
                'code': res.code,
                'msg': res.msg,
                'count': res.data.count,
                'data': res.data.list,
            }
        }
    });

    //模糊查询
    form.on('submit(*)', function (data) {
        // let jQuery = $('#search').serialize();
        // console.log(jQuery)
        let val = form.val('search');
        let roles = new Array()
        roles.length = 0;
        // console.log( $('input[name=role]:checkbox'))
        $('input[name=role]:checkbox').each(function (index, domEle) {
            if(this.checked==true){
                roles.push({
                    id: this.defaultValue
                })
            }
        })
        console.log(val.sex)
        if(val.sex === ''){
            console.log(roles)
            table.reload('user_table', {
                method: 'post',
                url: 'search/user',
                page: true,
                contentType: 'application/json',
                where: {
                    user: {
                        uname: val.uname,
                        email: val.email,
                        id: val.id,
                        phone: val.phone,
                        roleList: roles
                    }
                },
                parseData: function (res) {
                    return {
                        'code': res.code,
                        'msg': res.msg,
                        'count': res.data.count,
                        'data': res.data.list,
                    }
                }
            })
        }else{
            table.reload('user_table', {
                method: 'post',
                url: 'search/user',
                page: true,
                contentType: 'application/json',
                where: {
                    user: {
                        uname: val.uname,
                        email: val.email,
                        sex: val.sex === '1'? 'true':'false',
                        id: val.id,
                        phone: val.phone,
                        roleList: roles
                    }
                },
                parseData: function (res) {
                    return {
                        'code': res.code,
                        'msg': res.msg,
                        'count': res.data.count,
                        'data': res.data.list,
                    }
                }
            })
        }
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });


    form.on('submit(res)', function (data) {
        tableIns.reload({
            url: 'user/selectAll'
        });
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

    // 头部工具栏事件
    table.on('toolbar(table_filter)', function (obj) {
        let data = obj.data;
        if ('add' === obj.event) {
            getTextBox(tableIns, {
                type: 0,
                area: ['500px', '550px'],
                content: $('#user-add-form').html()
            }, {
                url: 'user/add',
                formId: '#table-add-form'
            })

            form.render(null, 'table-add-form')
        } else if ('delAll' === obj.event) {
            //获取选中的数据
            let checkStatus = table.checkStatus(obj.config.id)
            let choseData = checkStatus.data;

            if (choseData.length === 0) {//如果没有选择
                layer.msg('请选择要删除的数据');
            } else {//选择了
                //再次确认
                layer.confirm("确定要删除吗", function (index) {
                    let delIdJson = '';
                    //生成restful数据
                    for (let i = 0; i < choseData.length; i++) {
                        if (i === choseData.length - 1) {
                            delIdJson += choseData[i].id;
                            break;
                        }
                        delIdJson += choseData[i].id + ',';
                    }
                    //发送get请求
                    $.get('user/delAll/' + delIdJson, function (data) {
                        if (0 === data.code) {
                            layer.msg('删除成功', {time: 2000});
                        } else {
                            layer.msg('删除失败', {time: 2000});
                        }
                        //重新加载表格
                        table.reload('user_table', {
                            method: 'get',
                            url: 'user/selectAll',
                        });
                    });
                    //关闭消息框
                    layer.close(index);
                })
            }
        }
    })

    //工具栏事件
    table.on('tool(table_filter)', function (obj) {
        let data = obj.data;
        if ('edit' === obj.event) {
            getTextBox(tableIns, {
                type: 1,
                area: ['450px', '450px'],
                content: $('#user-edit-form').html()
            }, {
                url: 'user/edit',
                formId: '#table-edit-form'
            })
            let rids = data.roleList.map(role => role.id);
            $.each(rids, (i, val)=>{
                let node = $('input[type="checkbox"][name^="rids"][value="'+val+'"]');
                if(node && node.length){
                    node[0].checked = true;
                    form.render();
                }
            })
            // let rids = data.roleList.map(role => role.id);
            form.val('table-edit-form', {
                'id': data.id,
                'uname': data.uname,
                'sex': data.sex,
                'phone': data.phone,
                'email': data.email,
                // 'rids': rids
            })

        } else if ('del' === obj.event) {
            layer.confirm("确定要删除吗", function (index) {
                console.log(index)
                let id = data.id;
                $.get('user/del/' + id, function (data) {
                    if (0 === data.code) {
                        layer.msg('删除成功', {time: 2000});
                    } else {
                        layer.msg('删除失败', {time: 2000});
                    }
                });
                //重新加载表格
                table.reload('user_table', {
                    method: 'get',
                    url: 'user/selectAll',
                });
                layer.close(index);
            })
        }
    })
})