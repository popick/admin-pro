

layui.config({
    version: '1606406696877' //为了更新 js 缓存，可忽略
});
layui.use(['laydate', 'laypage', 'layer', 'table', 'upload'], function () {
    var laydate = layui.laydate //日期
        , laypage = layui.laypage //分页
        , layer = layui.layer //弹层
        , table = layui.table //表格
        , element = layui.element //元素操作
    ///执行一个 table 实例
    var tabIns = table.render({
        elem: '#roleMes'
        , url: 'role/selectAllRole' //数据接口
        , title: '角色表'
        , page: true //开启分页
        , toolbar: '#toolbar' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
        , totalRow: true //开启合计行
        , cols: [[ //表头
            {type: 'checkbox', fixed: 'left'}
            , {title: '序号', width: 130, templet: '#orderNum'}
            , {
                field: 'rname', title: '角色名', width: 200, templet: function (res) {
                    if (res.id < 4) {
                        return res.rname + '<span class="layui-badge">默认</span>'
                    }else {
                        return res.rname;
                    }
                }
            }
            , {field: 'power', title: '角色权限值', sort: true, width: 150}
            , {field: 'description', title: '权限描述', width: 300}
            , {field: "tails", title: '总人数', width: 200,align:"center", templet: function (res) {
                    if (res.tails == null)
                        return null;
                    return res.tails.countpeople
                }
            }
            , {field: 'operation', title: '操作', width: 300, align: "center", templet: "#role_operation"}
        ]]
    });

    //分页
    laypage.render({
        elem: 'pageDemo' //分页容器的id
        , count: 100 //总页数
        , skin: '#1E9FFF' //自定义选中色值
        , skip: true //开启跳页
        , jump: function (obj, first) {
            if (!first) {
                layer.msg('第' + obj.curr + '页', {offset: 'b'});
            }
        }
    });

    //监听头工具栏事件
    table.on('toolbar(roleMes)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id)
            , data = checkStatus.data; //获取选中的数据
        switch (obj.event) {
            case 'add':
                // 打开一个小弹窗
                getTextBox(tabIns, {
                        area: ['500px', '450px']
                        , content: $("#role-edit-form").html()
                        , type: 0
                    },
                    {
                        url: "role/add"
                        , formId: "#table-edit-form"
                    });
                form.render();
                break;
            case 'del':
                if (data.length === 0) {
                    layer.msg('请至少选择一行');
                } else {
                    var idArray = new Array();
                    for (var i = 0; i < data.length; i++) {
                        idArray[i] = data[i].id;
                        if (data[i].id < 4) {
                            layer.confirm("默认角色不可被删除")
                            return;
                        }
                    }

                    layer.confirm("确定删除所选的数据吗", function (index) {

                        $.ajax({
                            url: "role/delManyById"
                            , type: "post"
                            , contentType: "application/json;charset=UTF-8"
                            , data: JSON.stringify(idArray)
                            , success: function (data) {
                                if (data.code == 0) {
                                    layer.msg("删除成功", {time: 2000});
                                    tabIns.reload();
                                } else {
                                    layer.msg("删除失败", {time: 2000});
                                    tabIns.reload();
                                }
                            },
                            error:function (msg){
                                //console.log(msg.responseJSON.message);
                                layer.confirm(msg.responseJSON.message, {time: 2000})
                            }
                        })

                    });

                }
                break;
        }
        ;
    });

    table.on("tool(roleMes)", function (obj) {
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
        var id = data.id;
        if (layEvent === 'del') { //删除
            layer.confirm('真的删除行么', function (index) {
                layer.close(index);
                //向服务端发送删除指令
                $.ajax({
                    success: function (data) {
                        if (data.msg === "成功") {
                            layer.msg("删除成功", {time: 1000});
                            obj.del();  //删除对应行（tr）的DOM结构，并更新缓存
                        } else
                            layer.confirm("删除失败", {time: 1000})

                    },
                    url: "role/del",
                    type: "post",
                    dataType: "json",
                    data: {
                        "id": id
                    },
                    error: function (msg) {
                        //console.log(msg.responseJSON.message);
                        layer.confirm(msg.responseJSON.message);
                    }
                });
            });
        } else if (layEvent === 'edit') { //编辑

            getTextBox(tabIns, {
                type: 1
                , area: ['500px', '450px']
                , content: $("#role-edit-form").html()
            }, {
                url: "role/edit"
                , formId: "#table-edit-form"
            });
            // 数据回显到表单
            form.val('table-edit-form', {
                "id": id
                , 'rname': data.rname
                , 'power': data.power
                , 'description': data.description

            });
            form.render();
        }
    });
});