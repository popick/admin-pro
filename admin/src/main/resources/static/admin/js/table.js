let table;
let layer;
let form;

layui.use(['table', 'layer', 'form'], function () {
    table = layui.table;
    layer = layui.layer;
    form = layui.form;
});

/**
 * 表单弹出框
 * @param tabIns ：表格实例
 * @param layerData 弹出框参数（Json对象） type: 0：add 1：edit
 *                                     area：表单大小
 *                                     content：表单内容  格式为 $('#id').html()
 *
 * @param formData  表格参数（Json对象） url: 请求地址
 *                                     formId：表单id  格式为 '#id'
 */
function getTextBox(tabIns, layerData, formData){
    let name = layerData.type === 1 ? '修改':'添加'
    layer.open({
        type: 1,
        title: name,
        area: layerData.area,
        id: 'id',
        btn: [name, '取消'],
        moveType: 0,
        content: layerData.content,
        yes: function (index, layero) {
            layui.$('#layui-layer'+index).find('.layui-layer-btn0').css("pointer-events","none");
            TextBoxOk(tabIns, index, name, formData)
        }
    });

}


function TextBoxOk(tabIns, index, name, formData){
    let serializeArray = $(formData.formId).serializeArray();
    for(let i = 1; i < serializeArray.length; i++){
        if("" === serializeArray[i].value){
            layer.msg(tabIns.config.cols[0][i+1].title+"不能为空");
            layui.$('#layui-layer'+index).find('.layui-layer-btn0').css("pointer-events","auto");
            return;
        }
    }
    $.ajax({
        async: false,
        type: 'POST',
        dataType: "json",
        url: formData.url,
        data: $(formData.formId).serialize(),
        success: function (data) {
            if (0 === data.code) {
                if(data.data){
                    layer.msg(name + "成功", {time: 2000})
                }else{
                    layer.msg(name + "用户名重复", {time: 2000})
                    layui.$('#layui-layer'+index).find('.layui-layer-btn0').css("pointer-events","auto");
                    return;
                }
            } else {
                layer.msg(name + "失败", {time: 2000})
            }
            layer.close(index);
            tabIns.reload();
        },
        error: function (msg) {
            layer.confirm(msg.responseJSON.message, {time:2000})
        }
    })

}

