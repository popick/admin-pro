<#assign contextPath = request.contextPath>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <base href="${contextPath}/">
    <base ${contextPath}/">
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <link rel="stylesheet" href="static/admin/css/my.css">
    <script src="static/admin/js/jquery-3.5.1.js"></script>
    <script src="static/layui/layui.js"></script>
    <script src="static/admin/js/echarts.js"></script>
    <script src="static/admin/js/data.js"></script>
    <script src="static/admin/js/loadChart.js"></script>
    <script src="static/admin/js/page/index.js"></script>

</head>

<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <!-- 头部导航栏 -->
    <div class="layui-header">
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-shrink-right"></i>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-website"></i>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-refresh"></i>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search">
            </li>
        </ul>

        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-notice"></i>
                    <span class="layui-badge-dot"></span>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-theme"></i>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-note"></i>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-screen-full"></i>
                </a>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <cite>name</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;">基本资料</a></dd>
                    <dd><a href="javascript:;">修改密码</a></dd>
                    <dd><a href="javascript:;">退出</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-more-vertical"></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- 左部导航栏 -->
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div class="layui-logo"><span>my layuiadmin</span></div>
            <ul class="layui-nav layui-nav-tree" lay-shrink="all" lay-filter="navTree">
                <li class="layui-nav-item">
                    <a href="javascript:;" lay-href="main">
                        <i class="layui-icon layui-icon-home"></i>
                        <cite>首页</cite>
                    </a>

                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">
                        <i class="layui-icon layui-icon-component"></i>
                        <cite>组件</cite>
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" lay-href="user">用户信息</a></dd>
                        <dd><a href="javascript:;" lay-href="role">角色信息</a></dd>
                    </dl>
                </li>
            </ul>
        </div>

    </div>



    <div class="layui-body" id="laybody">
        <div class="layui-fluid admin-pagetabs">

            <div class="layui-tab"  lay-allowclose="true" lay-filter="main_content" style="width: 100%">
                <div class="layui-icon layui-icon-prev admin-icon-control"></div>
                <ul class="layui-tab-title">
                    <li class="layui-icon layui-icon-home layui-this" lay-id='main' lay-allowClose="false">首页</li>
                </ul>
                <div class="layui-icon layui-icon-next admin-icon-control"></div>
                <div class="layui-icon layui-icon-down admin-icon-control"></div>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <iframe id="#mainiframe" scrolling="yes" src="page/main" frameborder="0" style="width:100%;height:1000px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>



</script>
</body>
</html>