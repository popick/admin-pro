<#assign contextPath = request.contextPath>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>主页</title>
    <base href="${contextPath}/">
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <link rel="stylesheet" href="static/admin/css/my.css">
    <script src="static/admin/js/jquery-3.5.1.js"></script>
    <script src="static/layui/layui.js"></script>
    <script src="static/admin/js/echarts.js"></script>
    <script src="static/admin/js/data.js"></script>
    <script src="static/admin/js/loadChart.js"></script>
    <script src="static/admin/js/page/main.js"></script>

</head>
<body>

    <div class="layui-rows" >
    <div class="layui-col-md8" style="padding: 10px 5px">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">快捷方式</div>
                    <div class="layui-card-body">
                        <div class="layui-carousel" id="id_admin-carousel1">
                            <div carousel-item>
                                <ul class="layui-row layui-col-space10 admin-card-main admin-text-center layui-this">
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs3">
                                        <a>
                                            <i class="layui-icon layui-icon-console"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="layui-row layui-col-space10 admin-card-main admin-text-center">
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li class="layui-col-md3">
                                        <a>
                                            <i class="layui-icon layui-icon-set"></i>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="layui-carousel-ind">
                                <ul>
                                    <li class="layui-this"></li>
                                    <li class></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">待办事项</div>
                    <div class="layui-card-body">
                        <div class="layui-carousel" id="id_admin-carousel2">
                            <div carousel-item>
                                <ul class="layui-row layui-col-space10 admin-card-main admin-text-left layui-this">
                                    <li class="layui-col-xs6">
                                        <a>
                                            <h3>待审评论</h3>
                                            <p><cite>66</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs6">
                                        <a>
                                            <h3>待审帖子</h3>
                                            <p><cite>12</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs6">
                                        <a>
                                            <h3>待审商品</h3>
                                            <p><cite>99</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs6">
                                        <a>
                                            <h3>待发货</h3>
                                            <p><cite>20</cite></p>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="layui-row layui-col-space10 admin-card-main admin-text-left ">
                                    <li class="layui-col-xs6">
                                        <a>
                                            <h3>待审友情连接</h3>
                                            <p><cite>5</cite></p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="layui-carousel-ind">
                                <ul>
                                    <li class="layui-this"></li>
                                    <li class></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">数据概览</div>
                    <div class="layui-card-body">
                        <div class="layui-carousel" id="id_admin-carousel3">
                            <div carousel-item class="admin-chart-show" id="chart1">
                                <div ></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div class="layui-carousel-ind">
                                <ul>
                                    <li class="layui-this"></li>
                                    <li class></li>
                                    <li class></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-col-md4" style="padding: 10px 5px">
        <div class="layui-card">
            <div class="layui-card-header">版本信息</div>
            <div class="layui-card-body layui-text">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <td>当前版本</td>
                        <td>v1.4.0 pro</td>
                    </tr>
                    <tr>
                        <td>基于框架</td>
                        <td>layui-v2.5.6</td>
                    </tr>
                    <tr>
                        <td>主要特色</td>
                        <td>单页面 / 响应式 / 清爽 / 极简</td>
                    </tr>
                    <tr>
                        <td>获取渠道</td>
                        <td>
                            <div class="layui-btn-container">
                                <a href="javascript:;" class="layui-btn layui-btn-danger">获取授权</a>
                                <a href="javascript:;" class="layui-btn">立即下载</a>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="layui-card admin-progress">
            <div class="layui-card-header">效果报告</div>
            <div class="layui-card-body">
                <div class="layui-progress" lay-showpercent="yes">
                    <h3>
                        转化率（日同比28%）
                        <span class="layui-edge layui-edge-top" lay-tips="增长" lay-offset="-20"></span>
                        ）
                    </h3>
                    <div class="layui-progress-bar" lay-percent="65%" style="width: 65%;">
                        <span class="layui-progress-text">65%</span>
                    </div>
                </div>
                <div class="layui-progress" lay-showpercent="yes">
                    <h3>
                        转化率（日同比11%）
                        <span class="layui-edge layui-edge-down" lay-tips="下降" lay-offset="-20"></span>
                        ）
                    </h3>
                    <div class="layui-progress-bar" lay-percent="32%" style="width: 32%;">
                        <span class="layui-progress-text">32%</span>
                    </div>
                </div>

            </div>
        </div>
        <div class="layui-card admin-progress">
            <div class="layui-card-header">实时监控</div>
            <div class="layui-card-body">
                <div class="layui-progress">
                    <h3>CPU使用率</h3>
                    <div class="layui-progress-bar" lay-percent="58%" style="width: 58%">
                        <span class="layui-progress-text">58%</span>
                    </div>
                </div>
                <div class="layui-progress">
                    <h3>内存占用率</h3>
                    <div class="layui-progress-bar" lay-percent="90%" style="width: 90%;">
                        <span class="layui-progress-text">90%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(function() {
        loadChart('chart1', optionchartBing);
    })
</script>
</html>