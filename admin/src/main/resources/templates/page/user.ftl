<#assign contextPath = request.contextPath>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户信息表格</title>
    <base href="${contextPath}/">
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <link rel="stylesheet" href="static/admin/css/my.css">
    <script src="static/admin/js/jquery-3.5.1.js"></script>
    <script src="static/layui/layui.js"></script>
    <script src="static/admin/js/table.js"></script>
    <script src="static/admin/js/page/user.js"></script>

    <style>
        .layui-form-item {
            padding-right: 50px;
        }

        .layui-form-select dl {
            z-index: 9999;
        }
    </style>
</head>
<body>



    <#--头部模糊查询栏-->
    <div class="layui-row">
        <div class="layui-col-md10 layui-col-sm-offset1">
            <form class="layui-form" style="margin-top: 20px" lay-filter="search" id="search">
                <div class="layui-form-item">
                    <label class="layui-form-label">ID</label>
                    <div class="layui-input-inline">
                        <input type="text" name="id"
                               placeholder="请输入"
                               autocomplete="off" class="layui-input" style="width: 200px">
                    </div>
                    <label class="layui-form-label">姓名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="uname" placeholder="请输入"
                               autocomplete="off" class="layui-input" style="width: 200px">
                    </div>
                    <label class="layui-form-label">邮箱</label>
                    <div class="layui-input-inline">
                        <input type="text" name="email"
                               placeholder="请输入"
                               autocomplete="off" class="layui-input" style="width: 200px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">性别</label>
                    <div class="layui-input-inline">
                        <select name="sex" lay-verify="">
                            <option value="">不限</option>
                            <option value="1">男</option>
                            <option value="0">女</option>
                        </select>
                    </div>
                    <label class="layui-form-label">手机</label>
                    <div class="layui-input-inline">
                        <input type="text" name="phone"
                               placeholder="请输入"
                               autocomplete="off" class="layui-input" style="width: 200px">
                    </div>
                    <label class="layui-form-label">用户名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="username"
                               placeholder="请输入"
                               autocomplete="off" class="layui-input" style="width: 200px">
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">角色</label>
                        <div class="layui-input-inline" style="width: 600px;margin-top: 10px">
                            <#list roles as role>
                                <input type="checkbox" name="role" value="${role.id}" title="${role.rname}">
                            </#list>
                        </div>
                        <button lay-submit lay-filter="*" class="layui-btn layui-btn-lg"
                                style="margin-left: 50px;margin-top: 10px">
                            <i class="layui-icon layui-icon-search"></i></button>
                        <button lay-submit lay-filter="res" class="layui-btn layui-btn-lg"
                                style="margin-left: 10px;margin-top: 10px">
                            <i class="layui-icon layui-icon-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <#--表格-->
    <div class="layui-table" lay-filter="table_filter" id="user_table"></div>


<#--头部导航栏-->
<script type="text/html" id="admin_toolbar">
    <div class="layui-btn-container">
        <button class="layui-btn" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-danger" lay-event="delAll">删除选中</button>
    </div>
</script>

<#--工具栏-->
<script type="text/html" id="admin_tool">
    <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<#--add表单-->
<script type="text/html" id="user-add-form">
    <div style="margin-top: 30px">
        <form class="layui-form" id="table-add-form" lay-filter="table-add-form">
            <input type="hidden" name="id">

            <div class="layui-form-item">
                <label class="layui-form-label">用户名</label>
                <div class="layui-input-block">
                    <input type="text" name="username" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">密码</label>
                <div class="layui-input-block">
                    <input type="password" name="password" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="uname" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">性别</label>
                <div class="layui-input-block">
                    <input type="radio" name="sex" value="1" title="男" required>
                    <input type="radio" name="sex" value="0" title="女" required>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">手机</label>
                <div class="layui-input-block">
                    <input type="text" name="phone" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">邮箱</label>
                <div class="layui-input-block">
                    <input type="text" name="email" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">角色</label>
                <div class="layui-input-block">
                    <#list roles as role >
                        <input type="checkbox" name="rids" value="${role.id}" title="${role.rname}">
                    </#list>
                </div>
            </div>
        </form>
    </div>
</script>

<#--edit表单-->
<script type="text/html" id="user-edit-form">
    <div style="margin-top: 30px">
        <form class="layui-form" id="table-edit-form" lay-filter="table-edit-form">
            <input type="hidden" name="id">
            <div class="layui-form-item">
                <label class="layui-form-label">姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="uname" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">性别</label>
                <div class="layui-input-block">
                    <input type="radio" name="sex" value="1" title="男">
                    <input type="radio" name="sex" value="0" title="女">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">手机</label>
                <div class="layui-input-block">
                    <input type="text" name="phone" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">邮箱</label>
                <div class="layui-input-block">
                    <input type="text" name="email" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">角色</label>
                <div class="layui-input-block">
                    <#list roles as role>
                        <input type="checkbox" name="rids" value="${role.id}" title="${role.rname}">
                    </#list>
                </div>
            </div>
        </form>
    </div>
</script>
</body>
</html>
