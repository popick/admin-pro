<#assign contextPath = request.contextPath>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户权限表格</title>
    <base href="${contextPath}/">
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <link rel="stylesheet" href="static/admin/css/my.css">
    <script src="static/admin/js/jquery-3.5.1.js"></script>
    <script src="static/layui/layui.js"></script>
    <script src="static/admin/js/table.js"></script>
    <script src="static/admin/js/page/role.js"></script>
</head>
<body>
<#--表格-->
<table class="layui-hide" id="roleMes" lay-filter="roleMes"></table>

<#--表单内组件-->
<script type="text/html" id="role_operation">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<#--自增序号-->
<script type="text/html" id="orderNum">
    {{d.LAY_TABLE_INDEX+1}}
</script>

<#--表格外工具栏-->
<script type="text/html" id="toolbar">
    <div class="layui-inline" lay-event="add" id="add">
        <i class="layui-icon layui-icon-add-1"></i>
    </div>
    <div class="layui-inline" lay-event="del" id="del">
        <i class="layui-icon layui-icon-delete"></i>
    </div>

</script>

<#--edit表单-->
<script type="text/html" id="role-edit-form">
    <div style="margin: 30px;">
        <form class="layui-form" method="post" id="table-edit-form" lay-filter="table-edit-form">
            <input type="hidden" name="id">
            <div class="layui-form-item">
                <label class="layui-form-label">角色名</label>
                <div class="layui-input-block">
                    <input type="text" name="rname" required lay-verify="requried" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">角色权限值</label>
                <div class="layui-input-block">
                    <select id="power" name="power" lay-verify="required">
                        <option value="">角色权限值(值越大，权限越小)</option>
                        <#list 0..9 as t>
                            <option value="#{t}">#{t}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">角色描述</label>
                <div class="layui-input-block">
                    <textarea id="description" name="description" required lay-verify="requried" autocomplete="off"
                              class="layui-input" style="resize:none;height: 180px"></textarea>
                </div>
            </div>

        </form>
    </div>
</script>


</body>
</html>
