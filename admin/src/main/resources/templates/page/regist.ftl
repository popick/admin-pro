<#assign contextPath = request.contextPath>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Regist</title>
    <base href="${contextPath}/">
    <link rel="stylesheet" href="static/layui/css/layui.css">
    <script src="static/admin/js/jquery-3.5.1.js"></script>
    <script src="static/layui/layui.js"></script>
    <script src="static/admin/js/echarts.js"></script>
    <script src="static/admin/js/data.js"></script>
    <script src="static/admin/js/loadChart.js"></script>
</head>
<body>

<form class="layui-form" action="/logind/regist">
    <div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">
        <div class="layadmin-user-login-main">
            <div class="layadmin-user-login-box layadmin-user-login-header">
                <h2>FIVE BLACK</h2>
                <p>用户注册</p>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">用户名：</label>
                <div class="layui-input-inline">
                    <input type="text" name="username" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">密码：</label>
                <div class="layui-input-inline">
                    <input type="password" name="password" id="pwd" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">确认密码：</label>
                <div class="layui-input-inline">
                    <input type="password" name="password2"id="pwd2" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
                    <span id="tishi"></span>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">真实姓名：</label>
                <div class="layui-input-inline">
                    <input type="text" name="uname" required lay-verify="required" placeholder="请输入真实姓名" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">单选框：</label>
                <div class="layui-input-block">
                    <input type="radio" name="sex" value="1" title="男">
                    <input type="radio" name="sex" value="0" title="女" checked>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">电话号码：</label>
                <div class="layui-input-inline">
                    <input type="text" name="phone" required lay-verify="required" placeholder="请输入电话号码" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">EMAIL：</label>
                <div class="layui-input-inline">
                    <input type="text" name="email" required lay-verify="required" placeholder="请输入EMAIL" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" id="button" lay-submit lay-filter="formDemo">立即注册</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script src="/static/layui/layui.js"></script>
<script>
    layui.use("form",function (obj) {
        var form = layui.form;
        form.on("submit(formDemo)",function (event){
            var pwd = $("#pwd").val();
            var pwd1 = $("#pwd2").val();
            <!-- 对比两次输入的密码 -->
            if(pwd !== pwd1)
            {
                $("#tishi").html("两次密码不相同");
                $("#tishi").css("color","red")
                form.render();
                return false;
            }
            return true;
        });
    })
</script>
</body>
</html>