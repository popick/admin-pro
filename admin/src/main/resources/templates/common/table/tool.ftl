<script type="text/html" id="admin_toolbar">
    <div class="layui-btn-container">
        <button class="layui-btn" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-danger" lay-event="delAll">删除选中</button>
    </div>
</script>

<script type="text/html" id="admin_tool">
    <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>