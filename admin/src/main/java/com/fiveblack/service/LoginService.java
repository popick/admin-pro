package com.fiveblack.service;

import com.fiveblack.entity.User;

/**
 * @author yang
 * @date 2020/12/11 11:10
 */
public interface LoginService {

    User selectByName(String username, String password);

    int insertUandt(User user);

}
