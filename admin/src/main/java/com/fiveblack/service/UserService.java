package com.fiveblack.service;

import com.fiveblack.entity.Role;
import com.fiveblack.entity.User;
import org.beetl.sql.core.page.PageRequest;
import org.beetl.sql.core.page.PageResult;

import java.util.List;

/**
 * @author WAN
 *
 * 用户表的复杂操作（联表和单表）
 */
public interface UserService {
    List<User> selectAll(int page, int pageCount);

    boolean updateUserInfo(User user, Long[] rids);

    boolean insertUserInfo(User user, Long[] rids);

    /**
     * 删除通过id删除user表并删除user_role表中id
     *
     * @param userId
     * @return
     */
    boolean delUserById(Long userId);

    boolean delChoseUserById(Long[] ids);

    long allCount();


}
