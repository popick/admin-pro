package com.fiveblack.service.impl;
import com.fiveblack.entity.MiddleUserRole;
import com.fiveblack.entity.Role;
import com.fiveblack.entity.User;
import com.fiveblack.mapper.UserMapper;
import com.fiveblack.service.RoleService;
import com.fiveblack.service.UserRoleService;
import com.fiveblack.mapper.UserRoleMapper;
import com.fiveblack.service.UserService;
import com.fiveblack.util.PageUtil;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.beetl.sql.core.page.PageRequest;
import org.beetl.sql.core.page.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

/**
 * @author WAN
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleService roleService;

    @Override
    public List<User> selectAll(int page, int pageCount) {
        List<User> users = userMapper.selectAllUser(page,pageCount);

        if(users != null) {
            /**
             *这里userid 为空     不知道咋改
             */
            for (User user : users) {
                List<Role> roleList = user.getRoleList();
                if (roleList != null && roleList.size() == 1 && roleList.get(0).getId() == null) {
                    user.setRoleList(roleService.baseRoleList());
                }
            }
        }
        return users;
    }

    @Override
    public boolean updateUserInfo(User user, Long[] rids) {
        Long uid = user.getId();
        System.out.println(uid);
        userMapper.updateTemplateById(user);

        userRoleService.delByUid(uid);

        List<MiddleUserRole> list = new ArrayList<>();
        for(long rid : rids){
            list.add(new MiddleUserRole(uid, rid));
        }
        userRoleService.insertBatch(list);

        return true;
    }

    @Override
    public boolean insertUserInfo(User user, Long[] rids) {
        if(userMapper.existUsername(user.getUsername()) != null){
            return false;
        }

        Date date = new Date();
        user.setUpdateTime(date);
        user.setCreateTime(date);
        userMapper.upsert(user);
        if(rids != null){
            List<MiddleUserRole> middleUserRoles = new ArrayList<>();
            for (long rid : rids) {
                middleUserRoles.add(new MiddleUserRole(user.getId(), rid));
            }
            userRoleService.insertBatch(middleUserRoles);
        }

        return true;
    }

    @Override
    public boolean delUserById(Long userId) {
        try {
            userMapper.deleteById(userId);
            MiddleUserRole userRole = new MiddleUserRole();
            userRole.setUserId(userId);
            List<MiddleUserRole> template = userRoleService.template(userRole);
            userRoleService.delByUnionIds(template);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean delChoseUserById(Long[] ids) {
        for(long id : ids){
            if(!delUserById(id)){
                return false;
            }
        }
        return true;
    }

    @Override
    public long allCount() {
        return userMapper.allCount();
    }

}
