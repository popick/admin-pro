package com.fiveblack.service.impl;


import com.fiveblack.commoin.result.DataResult;
import com.fiveblack.mapper.RoleMapper;
import com.fiveblack.entity.Role;

import com.fiveblack.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author wangyuanfei
 * @date 2020/12/916:18
 */
@Transactional
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;

    @Override
    public List<Role> selectAllRoleMes() {
        List<Role> list ;
        try {
            list = roleMapper.selectAllRoleMess();
        } catch (Exception e) {
            throw new RuntimeException("网络繁忙,请稍后。。。");
        }

        return list;
    }

    @Override
    public boolean deleteRoleById(int id) {
        try {
            roleMapper.deleteUserRoleInfoByRoleId(id);
            roleMapper.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("网络繁忙,请稍后。。。");
        }

        return true;
    }


    @Override
    public boolean insertRoleByRole(Role role) {
        boolean flag = false;
        try {
            // 添加一个新角色
            Date date = new Date();
            role.setCreateTime(date);
            role.setUpdateTime(date);
            int count = roleMapper.selectByRname(role.getRname());
            if (count > 0){
                return flag;
            }
            roleMapper.insertTemplate(role);
            flag = !flag;

        } catch (Exception e) {
            throw new RuntimeException("网络繁忙,请稍后。。。");
        }
        return flag;
 }

    @Override
    public boolean updateRoleById(Role role) {

        try {
            Date date = new Date();
            role.setUpdateTime(date);
            roleMapper.updateTemplateById(role);
        } catch (Exception e) {
            throw new RuntimeException("网络繁忙,请稍后。。。");
        }
        return true;
    }

    @Override
    public boolean delManyRoleById(List<Integer> list) {
        try {
            roleMapper.delManyRoleAndUserByRoleId(list);
            roleMapper.delManyRoleById(list);
        } catch (Exception e) {
            throw new RuntimeException("网络繁忙,请稍后。。。");
        }
       return true;
    }

    @Override
    public List<Role> baseRoleList(){
        ArrayList<Role> roleList = new ArrayList<>();
        Role roleBase = new Role();
        roleBase.setRname("游客");
        roleBase.setPower(1000);
        roleBase.setDescription("没有赋予角色的用户");
        roleList.add(roleBase);
        return roleList;
    }

    @Override
    public List<Role> findRoleByUserId(Long id){
        return roleMapper.findRoleByUserId (id);
    }
}
