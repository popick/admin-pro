package com.fiveblack.service.impl;

import com.fiveblack.entity.Role;
import com.fiveblack.entity.User;
import com.fiveblack.mapper.LoginMapper;
import com.fiveblack.service.LoginService;
import com.fiveblack.service.RoleService;
import com.fiveblack.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.CustomSQLErrorCodesTranslation;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yang
 * @date 2020/12/11 11:09
 */

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private RoleService roleService;

    @Autowired
    private LoginMapper loginMapper;
    /**
     * @Author:66666
     */
    @Autowired
    private HttpServletRequest request;

    @Override
    public User selectByName(String username, String password) {
        User user = loginMapper.selectByName (username, password);
        if (user != null) {
            List < Role > roleList = roleService.findRoleByUserId (user.getId ());
            Map < Integer, String > map = new HashMap <> (roleList.size ( ));
            for (Role r : roleList) {
                map.put (r.getPower ( ), r.getRname ( ));
            }
            request.getSession ( ).setAttribute ("power", map);
        }
        return user;
    }

    @Override
    public int insertUandt(User user) {
        Date date = new Date ( );
        user.setUpdateTime (date);
        user.setCreateTime (date);
        int i = loginMapper.insertUandt (user);
        return i;
    }
}
