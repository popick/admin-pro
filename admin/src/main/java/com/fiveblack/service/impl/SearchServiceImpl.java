package com.fiveblack.service.impl;

import com.fiveblack.entity.User;
import com.fiveblack.mapper.SearchMapper;
import com.fiveblack.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author fengxin
 */
@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    SearchMapper searchMapper;

    @Override
    public List<User> search(User user,int page,int pageCount) {
        return searchMapper.search(user,page,pageCount);
    }

    @Override
    public List<User> searchAndRole(String[] rids,int size,User user,int page,int pageCount) {
        return searchMapper.searchAndRole(rids,size,user,page,pageCount);
    }
}
