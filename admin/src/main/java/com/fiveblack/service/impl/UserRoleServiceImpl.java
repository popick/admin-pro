package com.fiveblack.service.impl;

import com.fiveblack.entity.MiddleUserRole;
import com.fiveblack.mapper.UserRoleMapper;
import com.fiveblack.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author WAN
 * UserRoleService 实现类
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public void delByUnionIds(List<MiddleUserRole> middleUserRoles){
        for (MiddleUserRole middleUserRole : middleUserRoles) {
            delByUnionId(middleUserRole);
        }
    }

    @Override
    public void delByUnionId(MiddleUserRole middleUserRole){
        userRoleMapper.deleteById(middleUserRole);
    }

    @Override
    public List<MiddleUserRole> template(MiddleUserRole userRole) {
        System.out.println(userRoleMapper);
        return userRoleMapper.template(userRole);
    }

    @Override
    public void insertBatch(List<MiddleUserRole> middleUserRoles){
        userRoleMapper.insertBatch(middleUserRoles);
    }

    @Override
    public void insert(MiddleUserRole middleUserRole) {
        userRoleMapper.insert(middleUserRole);
    }

    @Override
    public void delByUid(long uid) {
        userRoleMapper.delbyUid(uid);
    }


}
