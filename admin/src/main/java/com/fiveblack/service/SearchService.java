package com.fiveblack.service;

import com.fiveblack.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author fengxin
 */
@Service
public interface SearchService {
    List<User> search(User user,int page,int pageCount);

    List<User> searchAndRole(String[] rids,int size,User user,int page,int pageCount);
}
