package com.fiveblack.service;

import com.fiveblack.entity.MiddleUserRole;
import com.fiveblack.mapper.UserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.websocket.server.ServerEndpoint;
import java.util.List;



/**
 * @author WAN
 *
 * 该类定义了中间表User_role一些操作，用于UserService 和 RoleService调用
 */
public interface UserRoleService {


   void delByUnionIds(List<MiddleUserRole> middleUserRoles);

   void delByUnionId(MiddleUserRole middleUserRole);

    List<MiddleUserRole> template(MiddleUserRole userRole);

    void insertBatch(List<MiddleUserRole> middleUserRoles);

    void insert(MiddleUserRole middleUserRole);

    void delByUid(long uid);
}
