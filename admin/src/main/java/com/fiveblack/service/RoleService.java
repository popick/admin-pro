package com.fiveblack.service;



import com.fiveblack.entity.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangyuanfei
 * @date 2020/12/916:17
 */
public interface RoleService {
    List<Role> selectAllRoleMes();

    boolean deleteRoleById(int id);

    boolean insertRoleByRole(Role role);

    boolean updateRoleById(Role roleBase);

    boolean delManyRoleById(List<Integer> integers);

    List<Role> baseRoleList();

    List<Role> findRoleByUserId(Long id);
}
