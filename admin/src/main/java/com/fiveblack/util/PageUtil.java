package com.fiveblack.util;

import java.util.List;

/**
 * @author fengxin
 */
public class PageUtil<T> {
    private List<T> list;
    private long count;

    public PageUtil() {
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
