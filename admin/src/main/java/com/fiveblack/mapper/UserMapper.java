package com.fiveblack.mapper;



import com.fiveblack.entity.User;
import org.beetl.sql.core.page.PageRequest;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;

import java.util.List;

/**
 * @author WAN
 */

@SqlResource(value="user")
public interface UserMapper extends BaseMapper<User> {

    List<User> selectAllUser(int page, int pageCount);

    PageResult<User> selectAllByPage(PageRequest<User> request);

    Long existUsername(String username);
}
