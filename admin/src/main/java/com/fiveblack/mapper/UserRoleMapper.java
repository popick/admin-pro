package com.fiveblack.mapper;

import com.fiveblack.entity.MiddleUserRole;
import org.beetl.sql.annotation.entity.LogicDelete;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Sql;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.annotation.Template;
import org.beetl.sql.mapper.annotation.Update;


public interface UserRoleMapper extends BaseMapper<MiddleUserRole> {
    @Sql("delete from user_role where user_id = ? and role_id <> 4")
    @Update
    void delbyUid(long uid);
}
