package com.fiveblack.mapper;

import com.fiveblack.entity.User;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.annotation.Update;
import org.springframework.test.context.jdbc.Sql;

import javax.annotation.Resource;

/**
 * @author yang
 * @date 2020/12/11 11:13
 */

@SqlResource(value = "login")
public interface LoginMapper extends BaseMapper<User> {

    User selectByName(String username, String password);

    @Update
    int insertUandt(User user);
}
