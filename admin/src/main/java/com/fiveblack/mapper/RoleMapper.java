package com.fiveblack.mapper;

import com.fiveblack.entity.Role;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Sql;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.annotation.Update;

import java.util.List;

/**
 * @author wangyuanfei
 * @date 2020/12/916:17
 */

@SqlResource(value = "role")
public interface RoleMapper extends BaseMapper<Role> {




    @Select
    List<Role> selectAllRoleMess();

    @Update
    int deleteUserRoleInfoByRoleId(int id);

    @Update
    int delManyRoleAndUserByRoleId(List<Integer> list);

    @Update
    int delManyRoleById(List<Integer> list);

    @Sql("select count(*) from role_info where rname like ?")
    @Select
    int selectByRname(String rname);

    @Select
    List< Role> findRoleByUserId(Long id);
}
