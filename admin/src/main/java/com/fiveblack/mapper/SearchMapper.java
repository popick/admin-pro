package com.fiveblack.mapper;

import com.fiveblack.entity.User;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;

import java.util.List;

/**
 * @author fengxin
 */
@SqlResource("search")
public interface SearchMapper extends BaseMapper<User> {
    List<User> search(User user,int page,int pageCount);

    List<User> searchAndRole(String[] rids,int size,User user,int page,int pageCount);
    long searchAndRoleCount(String[] rids,int size,User user,int page,int pageCount);
}
