package com.fiveblack.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fiveblack.commoin.result.DataResult;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @description: TODO
 * @author: 66666
 * @date: 17:06 2020/12/15
 */
@WebFilter("/user/*")
public class UserFilter implements Filter {
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String event = request.getRequestURI().split("/")[2];
        Map<Integer, String> power = (Map<Integer, String>) request.getSession().getAttribute(MyPowerConfig.powerName);

        if (MyPowerConfig.excludeUserRoute.equals(event) || power.containsKey(0)) {
            chain.doFilter(req, resp);
        } else {
            System.out.println ("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" );
            System.out.println ("==============================================" );
            System.out.println (this.getClass ().getName ()+"拦截了"+"user/"+event );
            System.out.println ("==============================================" );
            System.out.println ("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" );
            response.setCharacterEncoding("GBK");
            DataResult <String> dataResult = new DataResult<>(1,"",1L,"请先登录");
            response.getWriter().write(objectMapper.writeValueAsString(dataResult));
        }

    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

}
