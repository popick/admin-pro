package com.fiveblack.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @description: TODO
 * @author: 66666
 * @date: 11:12 2020/12/17
 */
public class MyPowerConfig {
    /**
     * 登陆时向session存储的权限值的名称
     */
    public static String powerName = "power";

    /**
     * 登录界面的URI
     */
    public static String loginRoute;

    /**
     * 注册界面的URI
     */
    public static String signRoute;

    /**
     * Role路径不需要拦截的方法名
     */
    public static String excludeRoleRoute;

    /**
     * User路径不需要拦截的方法
     */
    public static String excludeUserRoute=" ";

    static {
        Properties properties = new Properties ( );
        InputStream in = MyPowerConfig.class.getClassLoader ( ).getResourceAsStream ("filterPowerConfig.properties");
        try {
            properties.load (in);
            loginRoute = properties.getProperty ("loginRoute","");
            signRoute = properties.getProperty ("signRoute","");
            excludeUserRoute = properties.getProperty ("excludeUserRoute","");
            excludeRoleRoute = properties.getProperty ("excludeRoleRoute","");
        } catch (IOException e) {
            e.printStackTrace ( );
        }
    }
}
