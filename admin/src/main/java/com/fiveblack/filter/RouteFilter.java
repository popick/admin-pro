package com.fiveblack.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fiveblack.commoin.result.DataResult;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @description: TODO
 * @author: 66666
 * @date: 16:10 2020/12/16
 */
@WebFilter(value = {"/page/index", "/page/main", "/page/role", "/page/user"})
public class RouteFilter implements Filter {



    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse)resp;
        String[] s = request.getRequestURI().split ("/");
        String path = "/"+s[1]+"/"+s[2];
        if(MyPowerConfig.loginRoute.equals(path) || MyPowerConfig.signRoute.equals(path)){
            chain.doFilter(req,resp);
        }else{
            if (request.getSession ( ).getAttribute (MyPowerConfig.powerName) != null){
                chain.doFilter(req,resp);
            }else{
                System.out.println ("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" );
                System.out.println ("==============================================" );
                System.out.println (this.getClass ().getName ()+"拦截了"+path );
                System.out.println ("==============================================" );
                System.out.println ("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" );
                response.setCharacterEncoding("GBK");
                DataResult<String> dataResult = new DataResult<>(1,"",1L,"请先登录");
                ObjectMapper objectMapper = new ObjectMapper();
                response.getWriter().write(objectMapper.writeValueAsString(dataResult));
            }
        }

    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

}
