package com.fiveblack.route;


import com.fiveblack.entity.Role;
import com.fiveblack.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

/**
 * @author WAN
 */
@Controller
public class RouteController {

    @Autowired
    RoleService roleBaseService;

    @GetMapping("page/main")
    public String main(){
        return "page/main";
    }

    @GetMapping("page/user")
    public String user(Model model){
        List<Role> roleBases = roleBaseService.selectAllRoleMes();
        model.addAttribute("roles",roleBases);
        return "page/user";
    }

    @GetMapping("page/role")
    public String role() {
        return "page/role";
    }

    @GetMapping(value = {",", "/", "/index"})
    public String index() {
        return "page/index";
    }

    @GetMapping("page/login")
    public String login() {
        return "page/login";
    }

    @GetMapping("page/regist")
    public String register(){
        return "page/regist";
    }

}
