package com.fiveblack.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.beetl.sql.annotation.entity.*;
import org.beetl.sql.core.mapping.join.JsonConfigMapper;

import java.util.Date;
import java.util.List;

/**
 * @author WAN
 *
 * 仅用于与 Role 联表查询
 */

@EqualsAndHashCode(callSuper = true)
@ResultProvider(JsonConfigMapper.class)
@JsonMapper(resource = "mapper.UserAndRole")
@Table(name = "user_info")
public class User extends BaseEntity {
    private String username;
    private String password;
    private String uname;
    private Boolean sex;
    private String phone;
    private String email;
    private Date createTime;
    private Date updateTime;

    private List<Role> roleList;

    public User() {
    }


    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
