package com.fiveblack.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author fengxin
 */
@Component
public class SearchVo {
    private User user;
    private Integer page;
    private Integer limit;

    public SearchVo() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
