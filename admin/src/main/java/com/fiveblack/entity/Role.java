package com.fiveblack.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;


@Table(name="role_info")
public class Role extends BaseEntity {
    private String rname;
    private Integer power;
    private String description;
    private Date createTime;
    private Date updateTime;

    @Override
    public String toString() {
        return "Role{" +
                "rname='" + rname + '\'' +
                ", power=" + power +
                ", description='" + description + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

    public Role() {
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
