package com.fiveblack.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.AutoID;
import org.beetl.sql.core.TailBean;

/**
 * @author fengxin
 */
public class BaseEntity extends TailBean{
    @AutoID
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
