package com.fiveblack.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

@Table(name="user_role")
public class MiddleUserRole {
    @AssignID
    private Long userId;
    @AssignID
    private Long roleId;

    public MiddleUserRole(Long userId, Long roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public MiddleUserRole() {

    }
}
