package com.fiveblack.commoin.result;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author WAN
 */
@Data
@AllArgsConstructor
public class DataResult <T> {
    private Integer code;
    private String msg;
    private Long count;
    private T data;
}
