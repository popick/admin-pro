package com.fiveblack.controller;
import com.fiveblack.commoin.result.DataResult;
import com.fiveblack.entity.Role;
import com.fiveblack.entity.SearchVo;
import com.fiveblack.entity.User;
import com.fiveblack.mapper.SearchMapper;
import com.fiveblack.service.SearchService;
import com.fiveblack.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("search")
public class SearchResource {

    @Autowired
    SearchService searchService;

    @Autowired
    SearchMapper searchMapper;

    @PostMapping("user")
    @ResponseBody
    public DataResult<PageUtil<User>> getUser(@RequestBody SearchVo searchVo){
        User user = searchVo.getUser();
        System.out.println("查询用户"+user);
            List<User> search = searchService.search(user, searchVo.getPage(), searchVo.getLimit());
            PageUtil<User> pageUtil = new PageUtil<>();
            pageUtil.setList(search);
            pageUtil.setCount(searchMapper.allCount());
            return new DataResult<PageUtil<User>>(0, "请求成功", (long) search.size(), pageUtil);
    }
}
