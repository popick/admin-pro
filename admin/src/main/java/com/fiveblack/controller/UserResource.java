package com.fiveblack.controller;

import com.fiveblack.commoin.result.DataResult;
import com.fiveblack.entity.User;
import com.fiveblack.mapper.UserMapper;
import com.fiveblack.service.UserService;
import com.fiveblack.util.PageUtil;
import org.beetl.sql.core.page.DefaultPageRequest;
import org.beetl.sql.core.page.DefaultPageResult;
import org.beetl.sql.core.page.PageRequest;
import org.beetl.sql.core.page.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author WAN
 */
@RestController
@RequestMapping("user")
public class UserResource {
    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;


    @GetMapping("/selectAll")
    public DataResult selectAll(Integer page, Integer limit) {
        List<User> users = userService.selectAll(page, limit);
        PageUtil<User> pageUtil = new PageUtil<>();
        pageUtil.setList(users);
        pageUtil.setCount(userMapper.allCount());
        return new DataResult<PageUtil>(0, "请求成功", (long) users.size(), pageUtil);
    }

    @PostMapping("/edit")
    public DataResult<Boolean> edit(User user, Long[] rids) {
        boolean b = userService.updateUserInfo(user, rids);
        return new DataResult<>(0, b ? "请求成功" : "请求失败", 1L, b);

    }

    @PostMapping("/add")
    public DataResult<Boolean> add(User user, Long[] rids) {
        boolean b = userService.insertUserInfo(user, rids);

        return new DataResult<>(0, b ? "请求成功" : "请求失败", 1L, b);
    }

    @GetMapping("/del/{id}")
    public DataResult<Boolean> del(@PathVariable("id") Long id) {
        boolean b = userService.delUserById(id);
        return new DataResult<>(0, b ? "请求成功" : "请求失败", 1L, b);
    }

    @GetMapping("/delAll/{delIdJson}")
    public DataResult<Boolean> delChose(@PathVariable("delIdJson") Long[] ids) {
        boolean b = userService.delChoseUserById(ids);
        return new DataResult<>(0, b ? "请求成功" : "请求失败", 1L, b);
    }
}
