package com.fiveblack.controller;

import com.fiveblack.commoin.result.DataResult;
import com.fiveblack.entity.Role;
import com.fiveblack.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @author wangyuanfei
 * @date 2020/12/915:02
 */
@RequestMapping("role")
@RestController
public class RoleResource {

    @Autowired
    RoleService roleBaseService;

    /**
     * @param role 前端接受的新角色的信息 rname,power,descrption
     * @return DataResult    0：成功，“成功”,返回信息，count:操作的数据量，data:返回数据值
     */
    @PostMapping("/add")
    public synchronized DataResult<Boolean> addNewRole(Role role) {
        boolean flag = roleBaseService.insertRoleByRole(role);
        return new DataResult<>(0, "添加成功", 1L, flag);
    }

    /**
     *
     * @param id 前端传过来的要删除的id
     * @return code:成功返回的返回码,，flag 是操作码
     */
    @PostMapping("/del")
    public DataResult<Boolean> delRole(Integer id) {
        boolean flag = roleBaseService.deleteRoleById(id);
        return new DataResult<>(0, "成功", 0L, flag);
    }

    /**
     *
     * @param array 前端传过来的要删除的id 集
     * @return
     */
    @PostMapping("/delManyById")
    public DataResult<Boolean> delManyRoleById(@RequestBody List<Integer> array) {
        boolean flag = roleBaseService.delManyRoleById(array);
        return new DataResult<>(0, "成功", (long) 1, flag);
    }

    /**
     *
     * @param role 需要更新的角色信息
     * @return
     */
    @PostMapping("/edit")
    public DataResult<Boolean> updateRole(Role role) {
        boolean flag = roleBaseService.updateRoleById(role);
        return new DataResult<>(0, "成功", 1L, flag);
    }

    /**
     * @return dataResult格式的所有权限结果集
     */
    @GetMapping("/selectAllRole")
    public DataResult<List<Role>> selectAllRole() {
        List<Role> list = roleBaseService.selectAllRoleMes();

        return new DataResult<>(0, "成功", (long) list.size(), list);
    }

}

