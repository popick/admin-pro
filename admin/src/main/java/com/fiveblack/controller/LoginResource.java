package com.fiveblack.controller;

import com.fiveblack.commoin.result.DataResult;
import com.fiveblack.entity.User;
import com.fiveblack.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @author yang
 * @date 2020/12/11R 11:43
 */
@RequestMapping("logind")
@Controller
public class LoginResource {

    @Autowired
    private LoginService loginService;

    @GetMapping("/login")
    public String login(String username, String password) {
        User user = loginService.selectByName(username, password);
        System.out.println(user);
        if (user != null) {
            return "redirect:/";
        } else {
            return "page/login";
        }
    }

    @GetMapping("/regist")
    public String add(User user){
        int i = 0;
        i = loginService.insertUandt(user);
            return "page/login";
    }

}
